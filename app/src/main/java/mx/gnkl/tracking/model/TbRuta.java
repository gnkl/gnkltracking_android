package mx.gnkl.tracking.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbRuta implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nombre;
    private int estatus;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private TbTipoenvio idEnvio;
    private Integer isPausa;
    private Set<TbArchivos> archivos;

    public TbRuta() {
    }

    public TbRuta(Integer id) {
        this.id = id;
    }

    public TbRuta(Integer id, String nombre, int estatus, Date fechaCreacion) {
        this.id = id;
        this.nombre = nombre;
        this.estatus = estatus;
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRuta)) {
            return false;
        }
        TbRuta other = (TbRuta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TbRuta[ id=" + id + " ]";
    }

    /**
     * @return the fechaModificacion
     */
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    /**
     * @param fechaModificacion the fechaModificacion to set
     */
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the isPausa
     */
    public Integer getIsPausa() {
        return isPausa;
    }

    /**
     * @param isPausa the isPausa to set
     */
    public void setIsPausa(Integer isPausa) {
        this.isPausa = isPausa;
    }

    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }

}
