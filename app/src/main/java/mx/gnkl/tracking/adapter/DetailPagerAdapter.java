package mx.gnkl.tracking.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import tracking.gnkl.mx.gnkltracking.DetailBitacoraFragment;
import tracking.gnkl.mx.gnkltracking.EstatusListFragment;
import tracking.gnkl.mx.gnkltracking.TabFragment1;
import tracking.gnkl.mx.gnkltracking.TabFragment2;

/**
 * Created by jmejia on 26/12/16.
 */

public class DetailPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public DetailPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                DetailBitacoraFragment tab1 = new DetailBitacoraFragment();
                return tab1;
            case 1:
                EstatusListFragment tab2 = new EstatusListFragment();
                return tab2;
            /*case 2:
                TabFragment3 tab3 = new TabFragment3();
                return tab3;*/
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
