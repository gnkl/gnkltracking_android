package mx.gnkl.tracking.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbBitacora implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nombre;
    private Date fecha;
    private TbPuntoEo ptoDestino;
    private TbPuntoEo ptoOrigen;
    private TbVehiculo idVehiculo;
    private TbUsuarios idOperador;
    private TbUsuarios idUsuario;
    private TbBitacoraStatus idStatus;
    private Integer kmInicial;
    private Integer kmFinal;
    private Date horaSalida;
    private Date horaLlegada;
    private TbTipoenvio idEnvio;
    private Date fechaCreacion;
    private Set<TbArchivos> archivos;
    private TbRuta idRuta;
    private Integer isFinal;
    private Date fechaFin;
    private Integer isPausa;
    private Integer activo;

    public TbBitacora() {
    }

    public TbBitacora(Integer id) {
        this.id = id;
    }

    public TbBitacora(Integer id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TbPuntoEo getPtoDestino() {
        return ptoDestino;
    }

    public void setPtoDestino(TbPuntoEo ptoDestino) {
        this.ptoDestino = ptoDestino;
    }

    public TbPuntoEo getPtoOrigen() {
        return ptoOrigen;
    }

    public void setPtoOrigen(TbPuntoEo ptoOrigen) {
        this.ptoOrigen = ptoOrigen;
    }

    public TbVehiculo getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(TbVehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public TbUsuarios getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(TbUsuarios idOperador) {
        this.idOperador = idOperador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacora)) {
            return false;
        }
        TbBitacora other = (TbBitacora) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacora[ id=" + id + " ]";
    }

    /**
     * @return the idUsuario
     */
    public TbUsuarios getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(TbUsuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idStatus
     */
    public TbBitacoraStatus getIdStatus() {
        return idStatus;
    }

    /**
     * @param idStatus the idStatus to set
     */
    public void setIdStatus(TbBitacoraStatus idStatus) {
        this.idStatus = idStatus;
    }

    /**
     * @return the kmInicial
     */
    public Integer getKmInicial() {
        return kmInicial;
    }

    /**
     * @param kmInicial the kmInicial to set
     */
    public void setKmInicial(Integer kmInicial) {
        this.kmInicial = kmInicial;
    }

    /**
     * @return the kmFinal
     */
    public Integer getKmFinal() {
        return kmFinal;
    }

    /**
     * @param kmFinal the kmFinal to set
     */
    public void setKmFinal(Integer kmFinal) {
        this.kmFinal = kmFinal;
    }

    /**
     * @return the horaSalida
     */
    public Date getHoraSalida() {
        return horaSalida;
    }

    /**
     * @param horaSalida the horaSalida to set
     */
    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    /**
     * @return the horaLlegada
     */
    public Date getHoraLlegada() {
        return horaLlegada;
    }

    /**
     * @param horaLlegada the horaLlegada to set
     */
    public void setHoraLlegada(Date horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the fechaCreacion
     */
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * @param fechaCreacion the fechaCreacion to set
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the idRuta
     */
    public TbRuta getIdRuta() {
        return idRuta;
    }

    /**
     * @param idRuta the idRuta to set
     */
    public void setIdRuta(TbRuta idRuta) {
        this.idRuta = idRuta;
    }

    /**
     * @return the isFinal
     */
    public Integer getIsFinal() {
        return isFinal;
    }

    /**
     * @param isFinal the isFinal to set
     */
    public void setIsFinal(Integer isFinal) {
        this.isFinal = isFinal;
    }

    /**
     * @return the fechaFin
     */
    public Date getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the isPausa
     */
    public Integer getIsPausa() {
        return isPausa;
    }

    /**
     * @param isPausa the isPausa to set
     */
    public void setIsPausa(Integer isPausa) {
        this.isPausa = isPausa;
    }

    /**
     * @return the activo
     */
    public Integer getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(Integer activo) {
        this.activo = activo;
    }

}
