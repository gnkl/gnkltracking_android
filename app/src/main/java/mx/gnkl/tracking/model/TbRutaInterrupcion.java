package mx.gnkl.tracking.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jmejia on 3/01/17.
 */

public class TbRutaInterrupcion implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Date fechaInicio;
    private Date fechaFin;
    private String observaciones;
    private TbRuta idRuta;
    private Integer status;

    public TbRutaInterrupcion() {
    }

    public TbRutaInterrupcion(Integer id) {
        this.id = id;
    }

    public TbRutaInterrupcion(Integer id, Date fechaInicio, Date fechaFin, String observaciones) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.observaciones = observaciones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public TbRuta getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(TbRuta idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRutaInterrupcion)) {
            return false;
        }
        TbRutaInterrupcion other = (TbRutaInterrupcion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbRutaInterrupcion[ id=" + id + " ]";
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
