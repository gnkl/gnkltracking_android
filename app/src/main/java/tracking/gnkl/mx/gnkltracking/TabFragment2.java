package tracking.gnkl.mx.gnkltracking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.gnkl.tracking.adapter.BitacoraItemAdapter;
import mx.gnkl.tracking.model.TbUsuarios;
import mx.gnkl.tracking.singleton.Session;


public class TabFragment2 extends Fragment {
    private ListView _bitacoraRcoleccionListView=null;
    private ArrayList<Map<String,Object>> listBitacoraRecoleccion=null;
    private ProgressDialog progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.dialogProgressBarLoading();
        return inflater.inflate(R.layout.tab_fragment2, container, false);
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        Log.d("on  resume","on resume fragment recoleccion");
        new BitacoraRecoleccionListTask(getActivity(),1).execute();
    }


    public void dialogProgressBarLoading(){
        progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(true);
        progressBar.setMessage("Procesando ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private class BitacoraRecoleccionListTask extends AsyncTask<Void, Void, ArrayList<Map<String, Object>>> {

        private Context ctx;
        private Integer tipoEnvio;

        BitacoraRecoleccionListTask(Context ctx) {
            this.ctx =ctx;
            this.tipoEnvio=3;
        }

        BitacoraRecoleccionListTask(Context ctx, Integer tipoEnvio) {
            this.ctx =ctx;this.tipoEnvio=tipoEnvio;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected ArrayList<Map<String, Object>> doInBackground(Void... params) {
            try {
                Session session = Session.getInstance();
                TbUsuarios loggedUser = session.getLoggedUser();

                final String url = getString(R.string.base_uri)+"/android/list_bitacora_tipo_operador?iduser="+loggedUser.getIdUsu()+"&tipo="+this.tipoEnvio;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ArrayList<Map<String, Object>> result = restTemplate.getForObject(url,ArrayList.class);
                System.out.println(result);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String, Object>> listResult) {
            progressBar.dismiss();
            if(listResult!=null && !listResult.isEmpty()){
                listBitacoraRecoleccion=listResult;
                _bitacoraRcoleccionListView = (ListView) getView().findViewById(R.id.list_view_recoleccion);
                BitacoraItemAdapter adapter = new BitacoraItemAdapter(getActivity(), listResult);
                _bitacoraRcoleccionListView.setAdapter(adapter);
                _bitacoraRcoleccionListView.setOnItemClickListener(new ListClickHandler());
            }else{
                new AlertDialog.Builder(this.ctx).setTitle("Notificación Listado de viajes").setMessage("No existen viajes para recolección asignados a usted, verifique la señal de telléfono o cuminiquese a sistemas.").setNeutralButton("Cerrar", null).show();
            }

        }

    }

    public class ListClickHandler implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
            // TODO Auto-generated method stub
            Log.d("position",String.valueOf(position));
            if(listBitacoraRecoleccion!=null && listBitacoraRecoleccion.size()>position){
                Map<String,Object> item = listBitacoraRecoleccion.get(position);
                HashMap<String,Object> itemBitacora = new HashMap<String, Object>(item);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                Integer id = (Integer) item.get("id");
                intent.putExtra("selected-item", id.toString());
                intent.putExtra("selected-item-map", itemBitacora);
                startActivity(intent);
                Log.d("position",(String)item.get("nombre"));
                //getActivity().finish();
            }
        }
    }
}
