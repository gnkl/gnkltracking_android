package mx.gnkl.tracking.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Map;

import tracking.gnkl.mx.gnkltracking.R;

/**
 * Created by jmejia on 22/12/16.
 */

public class BitacoraItemAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Map<String,Object>> mDataSource;

    public BitacoraItemAdapter(Context context, ArrayList<Map<String,Object>> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //1
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    //2
    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    //3
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Map<String,Object> recipe = (Map) getItem(position);
        View rowView = mInflater.inflate(R.layout.list_item_recipe, parent, false);
        Integer isFinal = (Integer) recipe.get("is_final");
        Integer numStatus = (Integer) recipe.get("num_status");
        String color = "#000000";
        if((isFinal==0 && numStatus==5) || (isFinal==1 && numStatus == 7)){
            color = "#dff0d8";
        } else if(numStatus == 1){
            color = "#f2dede";
        } else {
            color = "#fcf8e3";
        }

        rowView.setBackgroundColor(Color.parseColor(color));
        TextView titleTextView = (TextView) rowView.findViewById(R.id.recipe_list_title);
        TextView subtitleTextView = (TextView) rowView.findViewById(R.id.recipe_list_subtitle);
        TextView detailTextView = (TextView) rowView.findViewById(R.id.recipe_list_detail);
        titleTextView.setText((String) recipe.get("nombre"));
        subtitleTextView.setText((String) recipe.get("des_status"));
        detailTextView.setText((String) recipe.get("nombre_ruta"));
        return rowView;
    }

}
