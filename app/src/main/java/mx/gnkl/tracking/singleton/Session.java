package mx.gnkl.tracking.singleton;


import mx.gnkl.tracking.model.TbUsuarios;

/**
 * Created by jmejia on 22/12/16.
 */

public class Session {

    private static TbUsuarios loggedUser = null;

    private static Session sInstance               = null;

    private Session(TbUsuarios user) {
        loggedUser=user;
    }

    public static void init(TbUsuarios pContext) {
        sInstance = new Session(pContext);
    }

    public static Session getInstance() {
        return sInstance;
    }

    public static TbUsuarios getLoggedUser(){
        return loggedUser;
    }
}