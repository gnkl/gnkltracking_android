package tracking.gnkl.mx.gnkltracking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;

import mx.gnkl.tracking.model.TbBitacora;
import mx.gnkl.tracking.model.TbRutaInterrupcion;


public class DetailBitacoraFragment extends Fragment {

    private String idBitacora=null;
    private  ProgressDialog progressBar;
    private ArrayList<Image> images = new ArrayList<>();
    private int REQUEST_CODE_PICKER = 2000;
    private Button btn_camera = null;
    private Button btn_save_image = null;
    private TextView tv_url=null;
    private TbBitacora detailBitacora=null;



    public DetailBitacoraFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getActivity().getIntent().getExtras();


        this.dialogProgressBarLoading();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            idBitacora=id;
            new BitacoraTask(getActivity()).execute();
        }
        //Log.d("id ",idBitacora);
    }

    public void startWithImagePicker(){
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap para seleccionar") // image selection title
                .single() // single mode
                .multi() // multi mode (default mode)
                .limit(3) // max images can be selected (999 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    public void dialogProgressBarLoading(){
        progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(false);
        progressBar.setMessage("Procesando ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_bitacora, container, false);
        btn_camera = (Button) view.findViewById(R.id.btn_bitacora_camera);
        btn_save_image = (Button) view.findViewById(R.id.btn_add_image_bitacora);
        tv_url = (TextView) view.findViewById(R.id.tv_list_bitacora_images);

        if(btn_camera!=null){
            btn_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startWithImagePicker();
                }
            });
        }

        if(btn_save_image != null){
            btn_save_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(images!=null && !images.isEmpty()){
                        proccessUploadTask();
                    }
                }
            });
        }
        return view;
    }

    public void proccessUploadTask(){
        new AlertDialog.Builder(getActivity()).
                setTitle("Agregar imágenes").
                setMessage("Esta seguro de agregar estas imágenes al viaje?").
                setNegativeButton("Cancelar", null).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        new UploadImageTask(getActivity()).execute();
                    }
                }).
                show();

        Log.d("login", "Login");
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if( requestCode == REQUEST_CODE_PICKER && data != null ){
                images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                tv_url.setText(images.size()+" Imágenes para subir a este viaje. ");
            }
        }

    }

    private class BitacoraTask extends AsyncTask<Void, Void, TbBitacora> {
        Context ctx;
        BitacoraTask(Context ctx) {
            this.ctx =ctx;
        }
        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected TbBitacora doInBackground(Void... params) {
            try {

                final String url = getString(R.string.base_uri)+"/android/get_bitacora?idbitacora="+idBitacora;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                TbBitacora result = restTemplate.getForObject(url,TbBitacora.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(TbBitacora selectedBitacora) {
            progressBar.dismiss();
            detailBitacora=selectedBitacora;
            System.out.println("selectedBitacora: "+selectedBitacora);
            if(selectedBitacora != null){
                String actionMessageButton = "";
                Button btnInterruptRuta = (Button) getActivity().findViewById(R.id.btn_interrupt_ruta);
                TextView interruptedBitacoraText = (TextView) getActivity().findViewById(R.id.isBitacoraInterrupted);
                interruptedBitacoraText.setTypeface(null, Typeface.BOLD);

                Integer isFinal = selectedBitacora.getIsFinal();
                Integer numStatus = selectedBitacora.getIdStatus().getNumStatus();

                if((isFinal==0 && numStatus>=5) || (isFinal==1 && numStatus == 7)){
                    btnInterruptRuta.setVisibility(View.GONE);
                } else {
                    if(selectedBitacora.getIsPausa()!=null && selectedBitacora.getIsPausa()==0){
                        interruptedBitacoraText.setText("RUTA EN ACCIÓN");
                        actionMessageButton="Pausar la ruta";
                    } else {
                        interruptedBitacoraText.setText("RUTA EN PAUSA");
                        actionMessageButton="Reanudar la ruta";
                        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                        fab.hide();
                    }
                }




                btnInterruptRuta.setText(actionMessageButton);
                btnInterruptRuta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(getActivity()).
                                setTitle("Poner o quitar pausa").
                                setMessage("Esta seguro de esta acción?").
                                setNegativeButton("Cancelar", null).
                                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        new InterrumpirRutaTask(getActivity()).execute();
                                    }
                                }).
                                show();
                    }
                });
            }

            if(selectedBitacora!=null ){
                Log.d("bitacora ",selectedBitacora.getNombre());
                Log.d("bitacora id",selectedBitacora.getId()+" ");

                TextView idBitacoraText = (TextView) getActivity().findViewById(R.id.idBitacora);
                //System.out.println(idBitacoraText);
                idBitacoraText.setText(selectedBitacora.getId().toString());

                TextView nombreBitacoraText = (TextView) getActivity().findViewById(R.id.nombreBitacora);
                nombreBitacoraText.setTypeface(null, Typeface.BOLD);
                nombreBitacoraText.setText(selectedBitacora.getNombre());

                TextView operadorBitacoraText = (TextView) getActivity().findViewById(R.id.operador);
                operadorBitacoraText.setTypeface(null, Typeface.BOLD);
                operadorBitacoraText.setText(selectedBitacora.getIdOperador().getNombre()+" "+selectedBitacora.getIdOperador().getApellidoPat()+" "+selectedBitacora.getIdOperador().getApellidoMat());

                TextView tipoBitacoraText = (TextView) getActivity().findViewById(R.id.tipo);
                tipoBitacoraText.setTypeface(null, Typeface.BOLD);
                tipoBitacoraText.setText(selectedBitacora.getIdEnvio().getDesEnvio());

                TextView estatusBitacoraText = (TextView) getActivity().findViewById(R.id.estatus);
                estatusBitacoraText.setTypeface(null, Typeface.BOLD);
                estatusBitacoraText.setText(selectedBitacora.getIdStatus().getDesStatus());

                TextView vehiculoBitacoraText = (TextView) getActivity().findViewById(R.id.vehiculo);
                vehiculoBitacoraText.setTypeface(null, Typeface.BOLD);
                vehiculoBitacoraText.setText(selectedBitacora.getIdVehiculo().getNoEconomico()+" | "+selectedBitacora.getIdVehiculo().getPlacas()+" | "+selectedBitacora.getIdVehiculo().getMarca());


            }else{
                new AlertDialog.Builder(this.ctx).setTitle("Notificación detalle viaje").setMessage("Error al obtener datos del viaje, favor de verificar conexión de datos.").setNeutralButton("Cerrar", null).show();
            }

        }

    }

    private class UploadImageTask extends AsyncTask<Void, Void, String> {
        Context ctx;
        String comentarios="";
        Integer kilometraje=0;
        File fileImage = null;
        MultiValueMap<String, Object> mvm = null;
        File file = null;
        Integer idStatus = null;

        UploadImageTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String url = getString(R.string.base_uri)+"/android/addbitacoraimage";
                FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
                formHttpMessageConverter.setCharset(Charset.forName("UTF8"));

                RestTemplate restTemplate = new RestTemplate();

                restTemplate.getMessageConverters().add(formHttpMessageConverter);
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                //restTemplate.setInterceptors(Arrays.asList(new LoggingRequestInterceptor()));

                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

                if(images!=null && !images.isEmpty()){
                    for(Image image: images){
                        try{
                            MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
                            Resource file1 = new FileSystemResource(image.getPath());
                            map.add("file", file1);

                            HttpHeaders imageHeaders = new HttpHeaders();
                            imageHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

                            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                                    // Add query parameter
                                    //.queryParam("status", idStatus)
                                    .queryParam("idbitacora", Integer.valueOf(idBitacora));

                            HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, imageHeaders);

                            HttpEntity<String> request = new HttpEntity<String>(imageHeaders);
                            ResponseEntity<String> response = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, imageEntity, String.class);
                            System.out.println(response);
                            System.out.println(response.getStatusCode());
                        }catch(Exception ex ){
                            ex.printStackTrace();
                        }
                    }
                }
                //return response.toString();
                return "SUCCESS";
            }catch(Exception ex){
                ex.printStackTrace();
                return "ERROR";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar.dismiss();
            images = new ArrayList<>();
            tv_url.setText("");
            if(result!=null && result.equals("SUCCESS")){
                new AlertDialog.Builder(this.ctx).setTitle("Notificación carga imágenes").setMessage("la carga de imágenes se realizó de manera correcta.").setNeutralButton("Cerrar", null).show();
            }else{
                new AlertDialog.Builder(this.ctx).setTitle("Notificación carga imágenes").setMessage("Error en la carga de imágenes, favor de verificar la señal de teléfono, verificar que se tengan datos o comunicarse a sistemas.").setNeutralButton("Cerrar", null).show();
            }

        }
    }

    private class InterrumpirRutaTask extends AsyncTask<Void, Void, TbRutaInterrupcion> {
        Context ctx;

        InterrumpirRutaTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected TbRutaInterrupcion doInBackground(Void... params) {
            try {

                final String url = getString(R.string.base_uri)+"/android/interrumpirRuta?idusuario="+detailBitacora.getIdOperador().getIdUsu()+"&idruta="+detailBitacora.getIdRuta().getId()+"&comentario=test";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                TbRutaInterrupcion result = restTemplate.getForObject(url,TbRutaInterrupcion.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(TbRutaInterrupcion rutaInterrupcion) {
            progressBar.dismiss();
            System.out.println("ruta interrupcion");
            System.out.println(rutaInterrupcion);
            if(rutaInterrupcion != null){
                Button btnInterruptRuta = (Button) getActivity().findViewById(R.id.btn_interrupt_ruta);
                TextView interruptedBitacoraText = (TextView) getActivity().findViewById(R.id.isBitacoraInterrupted);
                interruptedBitacoraText.setTypeface(null, Typeface.BOLD);
                //is_pausa=0
                Integer isFinal = detailBitacora.getIsFinal();
                Integer numStatus = detailBitacora.getIdStatus().getNumStatus();


                if((isFinal==0 && numStatus>=5) || (isFinal==1 && numStatus == 7)){
                    btnInterruptRuta.setVisibility(View.GONE);
                } else {
                    if(rutaInterrupcion.getStatus()!=null && rutaInterrupcion.getStatus()==0){
                        interruptedBitacoraText.setText("RUTA EN PAUSA");
                        btnInterruptRuta.setText("Reanudar la ruta");
                        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                        fab.hide();
                    } else {
                        interruptedBitacoraText.setText("RUTA EN ACCIÓN");
                        btnInterruptRuta.setText("Pausar la ruta");
                        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                        fab.show();
                    }
                }
            }
        }

    }


}
