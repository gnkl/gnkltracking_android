package tracking.gnkl.mx.gnkltracking;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Map;

import mx.gnkl.tracking.adapter.HistoricoItemAdapter;


public class EstatusListFragment extends Fragment {

    private String idBitacora=null;
    private ListView listHistoricoBitacora=null;
    private ProgressDialog progressBar;

    public EstatusListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogProgressBarLoading();

        Bundle bundle = getActivity().getIntent().getExtras();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            idBitacora=id;
            //new BitacoraTask(getActivity()).execute();
            new HistoricoBitacoraTask(getActivity()).execute();
        }
        Log.d("id ",idBitacora);

    }

    public void dialogProgressBarLoading(){
        progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(false);
        progressBar.setMessage("Procesando ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_estatus_list, container, false);
    }

    private class HistoricoBitacoraTask extends AsyncTask<Void, Void, ArrayList<Map<String,Object>>> {
        Context ctx;

        HistoricoBitacoraTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected ArrayList<Map<String,Object>> doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri)+"/android/historico_bitacora?idbitacora="+idBitacora;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ArrayList<Map<String,Object>> result = restTemplate.getForObject(url,ArrayList.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String,Object>> response) {
            progressBar.dismiss();
            if(response != null ){
                Log.d("bitacora ",response.toString());
                System.out.println(response);
                listHistoricoBitacora = (ListView) getActivity().findViewById(R.id.list_view_historico_bitacora);
                HistoricoItemAdapter adapter = new HistoricoItemAdapter(this.ctx, response);
                listHistoricoBitacora.setAdapter(adapter);
            }
        }
    }

}
