package tracking.gnkl.mx.gnkltracking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.support.Base64;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.gnkl.tracking.helper.ImageUtils;
import mx.gnkl.tracking.helper.RealPathUtil;


public class UpdateStatusActivity extends AppCompatActivity {

    private String idBitacora = null;
    private Map<String, Object> _resultLastStatus = null;
    HashMap<String,Object> hashmapBitacora = null;
    //private ImageView imgView;
    private ProgressDialog dialog;

    EditText _observacionesText = null;
    EditText _kilometrajeText = null;
    Button _updateStatusButton = null;

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;


    private Bitmap mBitmap;
    //private Button btn_upload;
    private Button btn_camera;

    private final CharSequence[] items = {"Take Photo", "From Gallery"};

    //private ImageView iv_image;
    //private TextView tv_url;
    //private EditText et_response;
    private Uri selectedImageUri;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    private ProgressDialog progressBar;


    private ArrayList<Image> images = new ArrayList<>();

    private int REQUEST_CODE_PICKER = 2000;

    private TextView textViewListImages;

    String realPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_status);
        this.dialogProgressBarLoading();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewListImages = (TextView) findViewById(R.id.listImages);


        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            idBitacora=id;
            new CurrentStatusTask(this).execute();
        }

        hashmapBitacora = (HashMap<String, Object>) bundle.getSerializable("selected-bitacora");
        //System.out.println("hashmapBitacora  "+hashmapBitacora);

        if(checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
        }
        _updateStatusButton = (Button) findViewById(R.id.buttonUpdateStatus);
        _updateStatusButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updateStatusProccess();
            }
        });

        //iv_image = (ImageView) findViewById(R.id.iv_image);
        //btn_upload = (Button) findViewById(R.id.btn_upload);
        btn_camera = (Button) findViewById(R.id.btn_camera);
        //et_response = (EditText) findViewById(R.id.et_response);
        //tv_url = (TextView) findViewById(R.id.tv_url);
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooserDialog();
            }
        });

        if (selectedImageUri == null) {
            //btn_upload.setClickable(false);
        }

    }

    public void startWithImagePicker(){
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap para seleccionar") // image selection title
                .single() // single mode
                .multi() // multi mode (default mode)
                .limit(3) // max images can be selected (999 by default)
                .showCamera(false) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    public void dialogProgressBarLoading(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Procesando ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int extStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int wriStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (extStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (wriStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void openFileChooserDialog() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        startWithImagePicker();
                        break;
                    default:
                }
            }
        });
        builder.show();*/
        startWithImagePicker();
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(this, "Permission to use Camera", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            initCameraIntent();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("SMS and Location Services Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        System.out.println("requestCode: "+requestCode);
        System.out.println(permissions);
        System.out.println(grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initCameraIntent();
            } else {
                Toast.makeText(this, "Permission denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }*/

    /*private void initGalleryIntent() {
        //Intent intent = new Intent();
        //intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GALLERY);

        Intent intent = new Intent(this, ImagePickerActivity.class);

        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_MODE, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_MULTIPLE);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_LIMIT, 10);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SHOW_CAMERA, true);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES, images);
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_TITLE, "Album");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_TITLE, "Tap to select images");
        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_DIRECTORY, "Camera");
        startActivityForResult(intent, REQUEST_CODE_PICKER);

    }*/

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyHHdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }



    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            /*if (requestCode == REQUEST_CAMERA) {
                realPath = selectedImageUri.getPath();
                System.out.println(realPath);
            }*/
            /*else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                }

                Log.d(TAG, "real path: " + realPath);
            }*/
            //else if( requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null ){
            if( requestCode == REQUEST_CODE_PICKER && data != null ){
                images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
                /*System.out.println(images);
                StringBuilder sb = new StringBuilder();
                for (int i = 0, l = images.size(); i < l; i++) {
                    sb.append(images.get(i).getPath() + "\n");
                }
                System.out.println(sb.toString());*/
                textViewListImages.setText(images.size()+" Imagenes para subir en este estatus. ");
            }

            /*if(selectedImageUri!=null){
                mBitmap = ImageUtils.getScaledImage(selectedImageUri, this);
                setImageBitmap(mBitmap);
            }*/
        }

    }

//    private void setImageBitmap(Bitmap bm) {
//        iv_image.setImageBitmap(bm);
//        //btn_upload.setClickable(true);
//        //btn_upload.setOnClickListener(upload);
//    }


    private class CurrentStatusTask extends AsyncTask<Void, Void, Map<String, Object>> {
        Context ctx;
        CurrentStatusTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected Map<String, Object> doInBackground(Void... params) {
            try {

                final String url = getString(R.string.base_uri)+"/android/current_status?idbitacora="+idBitacora;
                System.out.println("url: "+url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Map<String, Object> result = restTemplate.getForObject(url,Map.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map<String, Object> mapResult) {
            progressBar.dismiss();
            if(mapResult!=null && !mapResult.isEmpty()){
                //Log.d("list result",mapResult.toString());
                //System.out.println(mapResult);
                _resultLastStatus = mapResult;
                TextView idBitacoraText = (TextView) findViewById(R.id.nextStatus);
                idBitacoraText.setTypeface(null, Typeface.BOLD);
                idBitacoraText.setText((String) mapResult.get("des_status"));

                TextView nombreBitacoraText = (TextView) findViewById(R.id.currentKilometraje);
                nombreBitacoraText.setTypeface(null, Typeface.BOLD);
                Integer currentKm = (Integer) mapResult.get("current_km");
                System.out.println("currentKm : "+currentKm);
                nombreBitacoraText.setText(currentKm.toString() );

                EditText kilometraje = (EditText) findViewById(R.id.kilometraje);
                //System.out.println("edit text : "+kilometraje);
                if(hashmapBitacora!=null){
                    Integer isFinal = (Integer) hashmapBitacora.get("is_final");
                    Integer numStatus = (Integer) mapResult.get("num_status");
                    if(isFinal==0 && (numStatus==2 || numStatus==3)){
                        kilometraje.setHint(currentKm.toString());
                    } else if(isFinal==1 && numStatus==7){
                        System.out.println("edit text : "+kilometraje);
                        kilometraje.setHint(currentKm.toString());
                    } else {
                        //kilometraje.setText(currentKm);
                        //kilometraje.setHint(currentKm);
                        kilometraje.setVisibility(View.GONE);
                    }
                }
            }else{
                _resultLastStatus = null;
            }

        }
    }

    public void updateStatusProccess() {
        new AlertDialog.Builder(this).
                setTitle("Actualizar Estatus").
                setMessage("Esta seguro de actualizar el estatus a este viaje?").
                setNegativeButton("Cancelar", null).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if (!validate()) {
                            return;
                        }
                        Log.d("logic","send data to the server");
                        new UpdateStatusTask(UpdateStatusActivity.this).execute();
                        new UploadImageTask(UpdateStatusActivity.this).execute();
                    }
                }).
                //setNeutralButton("No", null).
                show();

        Log.d("login", "Login");
    }

    public boolean validate() {
        boolean valid = true;

        _observacionesText = (EditText) findViewById(R.id.comentario);
        _kilometrajeText = (EditText) findViewById(R.id.kilometraje);

        String observaciones = _observacionesText.getText().toString();
        String kilometrajeTxt = _kilometrajeText.getText().toString();
        Integer km = 0;
        if(kilometrajeTxt!=null && !kilometrajeTxt.isEmpty()){
            km = new Integer(kilometrajeTxt);
        }
        //
        Log.d("km",kilometrajeTxt);
        Integer currentKm = (Integer) _resultLastStatus.get("current_km");

        if (observaciones.isEmpty()) {
            _observacionesText.setError("enter a valid comment");
            valid = false;
        } else {
            _observacionesText.setError(null);
        }


        if (km > 0 && km < currentKm) {
            _kilometrajeText.setError("el kilometraje debe ser mayor a: "+currentKm);
            valid = false;
        } else {
            _kilometrajeText.setError(null);
        }
        return valid;
    }

    private class UpdateStatusTask extends AsyncTask<Void, Void, Map<String, Object>> {
        Context ctx;
        String comentarios="";
        Integer kilometraje=0;

        UpdateStatusTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
            _observacionesText = (EditText) findViewById(R.id.comentario);
            _kilometrajeText = (EditText) findViewById(R.id.kilometraje);

            comentarios = _observacionesText.getText().toString();
            String kilometrajeTxt = _kilometrajeText.getText().toString();
            if(kilometrajeTxt!=null && !kilometrajeTxt.isEmpty()){
                kilometraje = new Integer(kilometrajeTxt);
            }


        }

        @Override
        protected Map<String, Object> doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri)+"/android/update_status?idbitacora="+idBitacora+"&comentario="+comentarios+"&km="+kilometraje;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Map<String, Object> result = restTemplate.getForObject(url,Map.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map<String, Object> mapResult) {
            progressBar.dismiss();
            if(mapResult!=null && mapResult.get("id")!=null){
                new AlertDialog.Builder(this.ctx).
                        setTitle("Notificación de Estatus").
                        setMessage("Actualización correcta de estatus.").
                        setNeutralButton("Cerrar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                finish();
                                Intent intent = new Intent(UpdateStatusActivity.this, DetailActivity.class);
                                intent.putExtra("selected-item", idBitacora.toString());
                                intent.putExtra("selected-item-map", hashmapBitacora);

                                startActivity(intent);
                            }
                        }).show();
            }else{
                new AlertDialog.Builder(this.ctx).setTitle("Notificación de Estatus").setMessage("Actualización incorrecta de estatus, verificar la señal de teléfono o comunicarse con sistemas.").setNeutralButton("Cerrar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        finish();
                        Intent intent = new Intent(UpdateStatusActivity.this, DetailActivity.class);
                        intent.putExtra("selected-item", idBitacora.toString());
                        startActivity(intent);
                    }
                }).show();
            }
        }
    }


    private class UploadImageTask extends AsyncTask<Void, Void, String> {
        Context ctx;
        String comentarios="";
        Integer kilometraje=0;
        File fileImage = null;
        MultiValueMap<String, Object> mvm = null;
        File file = null;
        Integer idStatus = null;

        UploadImageTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
            if(_resultLastStatus!=null && !_resultLastStatus.isEmpty()){
                idStatus = (Integer) _resultLastStatus.get("id_status");
            }

            //Uri uriFromPath = Uri.fromFile(new File(realPath));
            //file = new File(realPath);
            //System.out.println(file);
            //System.out.println(idBitacora);
            //mvm = new LinkedMultiValueMap<String, Object>();
            //mvm.add("file", file);
            //mvm.add("idBitacora", 1896);
            //TODO ver como obtener el id del historico correspondiente
            //mvm.add("status", 662);

        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String url = getString(R.string.base_uri)+"/android/addhistoricoimage";
                FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
                formHttpMessageConverter.setCharset(Charset.forName("UTF8"));

                RestTemplate restTemplate = new RestTemplate();

                restTemplate.getMessageConverters().add(formHttpMessageConverter);
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                //restTemplate.setInterceptors(Arrays.asList(new LoggingRequestInterceptor()));

                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

                if(images!=null && !images.isEmpty()){
                    for(Image image: images){
                        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
                        Resource file1 = new FileSystemResource(image.getPath());
                        map.add("file", file1);

                        HttpHeaders imageHeaders = new HttpHeaders();
                        imageHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

                        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                                // Add query parameter
                                .queryParam("status", idStatus)
                                .queryParam("idBitacora", Integer.valueOf(idBitacora));

                        HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, imageHeaders);

                        HttpEntity<String> request = new HttpEntity<String>(imageHeaders);
                        ResponseEntity<String> response = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, imageEntity, String.class);
                    }
                }
                //return response.toString();
                return "SUCCESS";
            }catch(Exception ex){
                ex.printStackTrace();
                return "ERROR";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar.dismiss();
            //TODO verificar si tenemos que hacer algo para verificar su correcto resultado
            if(result.equals("SUCCESS")){
                Toast. makeText(this.ctx, "Carga de imágenes correcta.", Toast.LENGTH_LONG).show();
                //new AlertDialog.Builder(this.ctx).setTitle("Notificación Estatus").setMessage("Carga de imágenes correcta.").setNeutralButton("Cerrar", null).show();
            }else{
                Toast. makeText(this.ctx, "Carga de imágenes incorrecta, verifique señal del teléfono o comuniquese a sistemas.", Toast.LENGTH_LONG).show();
                //new AlertDialog.Builder(this.ctx).setTitle("Notificación Estatus").setMessage("Carga de imágenes incorrecta, verifique señal del teléfono o comuniquese a sistemas.").setNeutralButton("Cerrar", null).show();
            }
        }
    }
}
