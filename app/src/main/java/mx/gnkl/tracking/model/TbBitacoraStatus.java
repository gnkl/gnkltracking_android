package mx.gnkl.tracking.model;

import java.io.Serializable;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbBitacoraStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private int numStatus;
    private String desStatus;
    private int idEmpresa;
    private String comentario;
    private TbTipoenvio idEnvio;

    public TbBitacoraStatus() {
    }

    public TbBitacoraStatus(Integer id) {
        this.id = id;
    }

    public TbBitacoraStatus(Integer id, int numStatus, String desStatus, int idEmpresa) {
        this.id = id;
        this.numStatus = numStatus;
        this.desStatus = desStatus;
        this.idEmpresa = idEmpresa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumStatus() {
        return numStatus;
    }

    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public String getDesStatus() {
        return desStatus;
    }

    public void setDesStatus(String desStatus) {
        this.desStatus = desStatus;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacoraStatus)) {
            return false;
        }
        TbBitacoraStatus other = (TbBitacoraStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacoraStatus[ id=" + id + " ]";
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

}
