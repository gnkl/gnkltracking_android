package tracking.gnkl.mx.gnkltracking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.ListView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.gnkl.tracking.adapter.BitacoraItemAdapter;
import mx.gnkl.tracking.model.TbUsuarios;
import mx.gnkl.tracking.singleton.Session;


public class TabFragment1 extends Fragment {
    private ListView _bitacoraEntregaListView=null;
    private ArrayList<Map<String,Object>> listBitacoraEntrega=null;
    private ProgressDialog progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.dialogProgressBarLoading();
        return inflater.inflate(R.layout.tab_fragment1, container, false);
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        Log.d("on  resume","on resume fragment entrega");
        new BitacoraEntregaListTask(getActivity(),3).execute();
    }


    public void dialogProgressBarLoading(){
        progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(true);
        progressBar.setMessage("Procesando ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private class BitacoraEntregaListTask extends AsyncTask<Void, Void, ArrayList<Map<String, Object>>> {

        private Context ctx;
        private Integer tipoEnvio;

        BitacoraEntregaListTask(Context ctx) {
            this.ctx =ctx;
            this.tipoEnvio=3;
        }

        BitacoraEntregaListTask(Context ctx, Integer tipoEnvio) {
            this.ctx =ctx;this.tipoEnvio=tipoEnvio;
        }

        @Override
        protected void onPreExecute() {
            progressBar.show();
        }

        @Override
        protected ArrayList<Map<String, Object>> doInBackground(Void... params) {
            try {
                Session session = Session.getInstance();
                TbUsuarios loggedUser = session.getLoggedUser();

                final String url = getString(R.string.base_uri)+"/android/list_bitacora_tipo_operador?iduser="+loggedUser.getIdUsu()+"&tipo="+this.tipoEnvio;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ArrayList<Map<String, Object>> result = restTemplate.getForObject(url,ArrayList.class);
                System.out.println(result);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String, Object>> listResult) {
            progressBar.dismiss();
            if(listResult!=null && !listResult.isEmpty()){
                listBitacoraEntrega=listResult;
                _bitacoraEntregaListView = (ListView) getView().findViewById(R.id.list_view_entrega);
                BitacoraItemAdapter adapter = new BitacoraItemAdapter(getActivity(), listResult);
                _bitacoraEntregaListView.setAdapter(adapter);
                _bitacoraEntregaListView.setOnItemClickListener(new ListClickHandler());
            }else{
                new AlertDialog.Builder(this.ctx).setTitle("Notificación Listado de viajes").setMessage("No existen viajes para entrega asignados a usted, verifique la señal de telléfono o cuminiquese a sistemas.").setNeutralButton("Cerrar", null).show();
            }

        }

    }

    public class ListClickHandler implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
            // TODO Auto-generated method stub
            Log.d("position",String.valueOf(position));
            if(listBitacoraEntrega!=null && listBitacoraEntrega.size()>position){
                Map<String,Object> item = listBitacoraEntrega.get(position);
                HashMap<String,Object> itemBitacora = new HashMap<String, Object>(item);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                Integer id = (Integer) item.get("id");
                intent.putExtra("selected-item", id.toString());
                intent.putExtra("selected-item-map", itemBitacora);
                startActivity(intent);
                Log.d("position",(String)item.get("nombre"));
                //getActivity().finish();
            }
        }
    }
}
