package mx.gnkl.tracking.model;

import java.io.Serializable;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbPuntoEo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String nombrePunto;
    private String direccion;
    private int estatus;
    private String grupo;
    private String cliente;
    private int entrega;
    private int origen;
    private String ciudad;
    private String estado;
    private String comision;
    private String telefono;
    private String rfc;
    private String cp;

    public TbPuntoEo() {
    }

    public TbPuntoEo(Integer id) {
        this.id = id;
    }

    public TbPuntoEo(Integer id, String nombrePunto, String direccion, int estatus, String grupo, String cliente, int entrega, int origen) {
        this.id = id;
        this.nombrePunto = nombrePunto;
        this.direccion = direccion;
        this.estatus = estatus;
        this.grupo = grupo;
        this.cliente = cliente;
        this.entrega = entrega;
        this.origen = origen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombrePunto() {
        return nombrePunto;
    }

    public void setNombrePunto(String nombrePunto) {
        this.nombrePunto = nombrePunto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getEntrega() {
        return entrega;
    }

    public void setEntrega(int entrega) {
        this.entrega = entrega;
    }

    public int getOrigen() {
        return origen;
    }

    public void setOrigen(int origen) {
        this.origen = origen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPuntoEo)) {
            return false;
        }
        TbPuntoEo other = (TbPuntoEo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gnkl.bitacora.persist.mavenproject2.TbPuntoEo[ id=" + id + " ]";
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the comision
     */
    public String getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(String comision) {
        this.comision = comision;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the cp
     */
    public String getCp() {
        return cp;
    }

    /**
     * @param cp the cp to set
     */
    public void setCp(String cp) {
        this.cp = cp;
    }

}
