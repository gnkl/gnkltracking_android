package tracking.gnkl.mx.gnkltracking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.HashMap;

import mx.gnkl.tracking.adapter.DetailPagerAdapter;

public class DetailActivity extends AppCompatActivity {

    private String idBitacora=null;
    private HashMap mapBitacora=null;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Detalle"));
        tabLayout.addTab(tabLayout.newTab().setText("Lista Estatus"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        final DetailPagerAdapter adapter = new DetailPagerAdapter (getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            idBitacora=id;
        }
        Log.d("id ",idBitacora);


        HashMap<String,Object> hashmapBitacora = (HashMap<String, Object>) bundle.getSerializable("selected-item-map");
        System.out.println(hashmapBitacora);
        if(hashmapBitacora!=null && !hashmapBitacora.isEmpty()){
            mapBitacora=hashmapBitacora;
            Integer isFinal = (Integer) mapBitacora.get("is_final");
            Integer numStatus = (Integer) mapBitacora.get("num_status");
            boolean shouldHide=false;
            if(isFinal==0 && numStatus>=5){
                shouldHide=true;
            } else if(isFinal==1 && numStatus == 7){
                shouldHide=true;
            }

            if(shouldHide==true){
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.hide();
            }else{
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("test","test");
                        Intent intent = new Intent(DetailActivity.this, UpdateStatusActivity.class);
                        intent.putExtra("selected-item", idBitacora);
                        intent.putExtra("selected-bitacora", mapBitacora);
                        startActivity(intent);
                        finish();
                    }
                });
            }

        }

    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        Log.d("on  resume","on resume detail activity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
